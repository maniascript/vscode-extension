Visual Studio Code MSLint extension
===================================

![Visual Studio Marketplace Version](https://img.shields.io/visual-studio-marketplace/v/aessi.vscode-mslint?label=Version)
![Visual Studio Marketplace Downloads](https://img.shields.io/visual-studio-marketplace/d/aessi.vscode-mslint?label=Downloads)
![Visual Studio Marketplace Installs](https://img.shields.io/visual-studio-marketplace/i/aessi.vscode-mslint?label=Install)
![Visual Studio Marketplace Rating](https://img.shields.io/visual-studio-marketplace/stars/aessi.vscode-mslint?label=Rating)

Integrates [MSLint](https://mslint.aessi.dev) into Visual Studio Code.

## Features

MSLint will lint your code and display the result directly inside VS Code.

![linting](doc/feature-lint.png)

## Configuration

If you are using a single root workspace, the extension will look for an `mslint.json` configuration file in the root of the workspace. See the [configuration file documentation](https://mslint.aessi.dev/guide/configuration-file) for all available options.

## Extension Settings

The extension offer the following settings:

* `mslint.lintOn`: Specifies when to lint the file. As you type, on save or on command only.
* `mslint.configFileName`: Name of the MSLint configuration file that the extension will search in the root of the workspace.
* `mslint.configFilePath`: Path to a configuration file to use instead of the one found in the root of the workspace.

## Release Notes

Check [CHANGELOG.md](CHANGELOG.md)
