Changelog
=========

2.1.0
-----

- [#20](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/20) Update dependencies

2.0.0
-----

- [#18](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/18) Upgraded the MSLint module to the 4.0.0 version

1.3.0
-----

- [#16](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/16) Upgraded the MSLint module to the 3.4.0 version
- [#17](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/17) Upgraded node dependencies

1.2.0
-----

- [#15](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/15) Upgraded the MSLint module to the 3.3.0 version

1.1.1
-----

- [#14](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/14) Redirected the badges in the readme file to the correct extension

1.1.0
-----

- [#12](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/12) Upgraded the MSLint module to the 3.2.1 version
- [#13](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/13) Removed diagnostic for disabled rules

1.0.0
-----

- [#11](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/11) Upgraded the MSLint module to the 3.1.0 version

0.7.0
-----

- [#10](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/10) Upgraded the MSLint module to the 0.9.0 version

0.6.0
-----

- [#9](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/9) Upgraded the MSLint module to the 0.8.0 version

0.5.0
-----

- [#8](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/8) Upgraded the MSLint module to the 0.7.0 version

0.4.0
-----

- [#6](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/6) Upgraded the MSLint module to the 0.6.1 version
- [#7](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/7) Added the ability to use a custom Maniascript API

0.3.1
-----

- Update MSLint to version 0.5.1

0.3.0
-----

- [#3](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/3) Added a setting to select when to lint
- [#4](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/4) Added a lint command

0.2.1
-----

- [#2](https://gitlab.com/maniascript/vscode-extension-mslint/-/issues/2) Fixed autocompletion error

0.2.0
-----

- Initial release
